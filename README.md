# README #

This installs plugins that are an enhancement to Janus.

https://github.com/carlhuda/janus

Once janus is configured, follow these instructions.

git clone --recurse-submodules git@gitlab.com:dutchrapley/vim-janus.git ~/.janus

add the following to ~/.gvimrc.after (create the file it it doesn't exist)

    if filereadable(expand("~/.janus/mappings.vim"))
        source ~/.janus/mappings.vim
    endif
