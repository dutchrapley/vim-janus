" vim-airline
  let g:airline_powerline_fonts = 1
  let g:airline_mode_map = {
      \ '__' : '-',
      \ 'n'  : 'N',
      \ 'i'  : 'I',
      \ 'R'  : 'R',
      \ 'c'  : 'C',
      \ 'v'  : 'V',
      \ 'V'  : 'V',
      \ '' : 'V',
      \ 's'  : 'S',
      \ 'S'  : 'S',
      \ '' : 'S',
      \ }
" Supertab code completion"
  let g:SuperTabContextDefaultCompletionType = "<c-n>"

" CtrlP
  nnoremap <Leader>b :<C-U>CtrlPBuffer<CR>
  nnoremap <Leader>t :<C-U>CtrlP<CR>
  nnoremap <Leader>T :<C-U>CtrlPTag<CR>

  " respect the .gitignore
  let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . --cached --exclude-standard --others']


" silver searcher
nmap g/ :Ag!<space>
nmap g* :Ag! -w <C-R><C-W><space>
nmap ga :AgAdd!<space>
nmap gn :cnext<CR>
nmap gp :cprev<CR>
nmap gl :cwindow<CR>

" NERDTree
  nmap <leader>g :NERDTreeToggle<CR>
  nmap <leader>G :NERDTreeFind<CR>


" Tabular for aligning text
  function! CustomTabularPatterns()
    if exists('g:tabular_loaded')
      AddTabularPattern! symbols         / :/l0
      AddTabularPattern! hash            /^[^>]*\zs=>/
      AddTabularPattern! chunks          / \S\+/l0
      AddTabularPattern! assignment      / = /l0
      AddTabularPattern! comma           /^[^,]*,/l1
      AddTabularPattern! colon           /:\zs /l0
      AddTabularPattern! options_hashes  /:\w\+ =>/
    endif
  endfunction

  autocmd VimEnter * call CustomTabularPatterns()

  " shortcut to align text with Tabular
  map <Leader>a :Tabularize<space>

"ansible-vim
let g:ansible_attribute_highlight = "od"
let g:ansible_name_highlight = 'b'
let g:ansible_extra_keywords_highlight = 1
let g:ansible_unindent_after_newline = 1

" easy wrap toggling
nmap <Leader>w :set wrap!<cr>
nmap <Leader>W :set nowrap<cr>

" close all other windows (in the current tab)
nmap gW :only<cr>

" go to the alternate file (previous buffer) with g-enter
nmap g

" insert blank lines without going into insert mode
nmap go o<esc>
nmap gO O<esc>

" mapping the jumping between splits. Hold control while using vim nav.
nmap <C-J> <C-W>j
nmap <C-K> <C-W>k
nmap <C-H> <C-W>h
nmap <C-L> <C-W>l

" Yank from the cursor to the end of the line, to be consistent with C and D.
nnoremap Y y$

" select the lines which were just pasted
nnoremap vv `[V`]

" clean up trailing whitespace
map <Leader>c :StripTrailingWhitespaces<cr>

" compress excess whitespace on current line
map <Leader>e :s/\v(\S+)\s+/\1 /<cr>:nohl<cr>

" delete all buffers
map <Leader>d :bufdo bd<cr>

" map spacebar to clear search highlight
nnoremap <Leader><space> :noh<cr>

" buffer resizing mappings (shift + arrow key)
nnoremap <S-Up> <c-w>+
nnoremap <S-Down> <c-w>-
nnoremap <S-Left> <c-w><
nnoremap <S-Right> <c-w>>

" reindent the entire file
map <Leader>I gg=G``<cr>

" insert the path of currently edited file into a command
" Command mode: Ctrl-P
cmap <C-S-P> <C-R>=expand("%:p:h") . "/" <cr>

